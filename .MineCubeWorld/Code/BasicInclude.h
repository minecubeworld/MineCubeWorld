#include "INCLUDE\gl.h"
#include "INCLUDE\glu.h"
#include "INCLUDE\glaux.h"
#include "INCLUDE\glfw3.h"
#include <stdio.h>
#include <conio.h>

#ifdef _WIN32
#include <WinDef.h>
#include <Windows.h>
#endif

#define PI 3.1415926
#define MINECUBEWORLD_MODE_DEBUG

extern GLFWwindow * MainWindow;
extern int CurrentWidth;
extern int CurrentHeight;

void GetFPS();
int loadtextures();
void Player_Mouse();
void Player_Move();
void Draw_List_Cube(int Face);
void DrawMap();
GLuint Create_List();
void UpdateWindow(int Width, int Height);
void UpdateGame();
