#include "DrawCube.h"

void Cube_Face_1() {
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0, 1.0);
	glTexCoord2f(1.0, 0.0); glVertex3f(1.0, -1.0, 1.0);	
	glTexCoord2f(1.0, 1.0); glVertex3f(1.0, 1.0, 1.0);	
	glTexCoord2f(0.0, 1.0); glVertex3f(-1.0, 1.0, 1.0);	
	glEnd();
}

void Cube_Face_2() {
	glBegin(GL_QUADS);
	glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, -1.0, -1.0);
	glTexCoord2f(1.0, 1.0); glVertex3f(-1.0, 1.0, -1.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(1.0, 1.0, -1.0);	
	glTexCoord2f(0.0, 0.0); glVertex3f(1.0, -1.0, -1.0);
	glEnd();
	Cube_Face_1();
}

void Cube_Face_3() {
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 1.0); glVertex3f(-1.0, 1.0, -1.0);
	glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, 1.0, 1.0);	
	glTexCoord2f(1.0, 0.0); glVertex3f(1.0, 1.0, 1.0);	
	glTexCoord2f(1.0, 1.0); glVertex3f(1.0, 1.0, -1.0);
	glEnd();
	Cube_Face_2();
}

void Cube_Face_4() {
	glBegin(GL_QUADS);
	glTexCoord2f(1.0, 1.0); glVertex3f(-1.0, -1.0, -1.0);
	glTexCoord2f(0.0, 1.0); glVertex3f(1.0, -1.0, -1.0);
	glTexCoord2f(0.0, 0.0); glVertex3f(1.0, -1.0, 1.0);	
	glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, -1.0, 1.0);
	glEnd();
	Cube_Face_3();
}

void Cube_Face_5() {
	glBegin(GL_QUADS);
	glTexCoord2f(1.0, 0.0); glVertex3f(1.0, -1.0, -1.0);
	glTexCoord2f(1.0, 1.0); glVertex3f(1.0, 1.0, -1.0);	
	glTexCoord2f(0.0, 1.0); glVertex3f(1.0, 1.0, 1.0);	
	glTexCoord2f(0.0, 0.0); glVertex3f(1.0, -1.0, 1.0);	
	glEnd();
	Cube_Face_4();
}

void Cube_Face_6() {
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-1.0, -1.0, -1.0);
	glTexCoord2f(1.0, 0.0); glVertex3f(-1.0, -1.0, 1.0);
	glTexCoord2f(1.0, 1.0); glVertex3f(-1.0, 1.0, 1.0);	
	glTexCoord2f(0.0, 1.0); glVertex3f(-1.0, 1.0, -1.0);
	glEnd();
	Cube_Face_5();
}
